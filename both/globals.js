Collections = {};

var imageStore = new FS.Store.GridFS("images");

Collections.Messages = new Mongo.Collection("message");

Collections.Images = new FS.Collection("images", {
    stores: [imageStore]
});