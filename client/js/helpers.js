"use strict";

Template.posts.helpers({
    postList: function () {
        return Collections.Messages.find({}, {
            sort: {
                createdAt: -1
            }
        });

    },

    updateMasonry: function () {
        $('.grid').imagesLoaded().done(function () {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                gutter: 10,
                columnWidth: '.grid-sizer',
                percentPosition: true
            });
        });
    },

    displayImages: function (imageId) {
        var image = Collections.Images.find({
            _id: imageId

        }).fetch();

        return image[0].url();
    }

});

Accounts.ui.config({
    passwordSignupFields: "USERNAME_ONLY"


});

//Template.name.helpers({
//  username: function () {
//    return Meteor.user().profile.first_name;
//}
//});