"use strict";
Template.form.events({

    'submit .add-new-post': function (event) {
        event.preventDefault();

        var postImage = event.currentTarget.children[2].children[0].children[1].files[0];

        var postName = event.currentTarget.children[0].value;

        var postMessage = event.currentTarget.children[1].value;

        if (postImage === "" || postMessage === "" || postName === "") {
            Materialize.toast("Incomplete", 4000);
            return false;
        }

        Collections.Images.insert(postImage, function (error, fileObject) {
            if (error) {
                //to-do ; inform the user that it failed
            } else {
                Collections.Messages.insert({
                    message: postMessage,
                    createdAt: new Date(),
                    name: postName,
                    imageId: fileObject._id
                });



                //to-do: insert post data right here
                //insert data name imageId message created by date
                // fileObject._id                        
                $('.grid').masonry('reloadItems');
            }
        });
    }
});